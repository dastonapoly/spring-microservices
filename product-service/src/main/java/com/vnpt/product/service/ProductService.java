package com.vnpt.product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vnpt.product.domain.Product;
import com.vnpt.product.repository.ProductRepository;


@Service
public class ProductService {

	@Autowired
    private ProductRepository productRepository;
	
	public Product findProductByName(String name) {
		return productRepository.findProductByNameIgnoreCase(name);
	}
    

}