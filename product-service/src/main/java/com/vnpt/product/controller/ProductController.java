package com.vnpt.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vnpt.product.domain.Product;
import com.vnpt.product.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productSrv;
	
	@GetMapping(value = "/find-product-by-name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Product findProductByName(@PathVariable("name") String name) {
        return productSrv.findProductByName(name);
    }
}
