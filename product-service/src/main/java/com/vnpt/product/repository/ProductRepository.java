package com.vnpt.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vnpt.product.domain.Product;


public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	public Product findProductByNameIgnoreCase(String name);
	
}