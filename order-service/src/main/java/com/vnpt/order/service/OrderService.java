package com.vnpt.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vnpt.order.domain.Order;
import com.vnpt.order.repository.OrderRepository;


@Service
public class OrderService {

	@Autowired
    private OrderRepository orderRepository;

    public enum OrderStatus {
        NEW, DONE, CANCELED
    }
	
	@Transactional
	public Order createOrder(Order order) {
		order.setStatus(OrderStatus.NEW);
		Order orderNew = orderRepository.save(order);
		return orderNew;
	}
    

}