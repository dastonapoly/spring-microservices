package com.vnpt.order.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.vnpt.order.domain.Product;

@FeignClient(name = "product-service")
public interface ProductServiceClient {
	
	@GetMapping(value = "/find-product-by-name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Product findProductByName(@PathVariable("name") String name);
	
}
