package com.vnpt.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vnpt.order.domain.Order;


public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	
}