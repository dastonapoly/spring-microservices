package com.vnpt.order.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vnpt.order.client.ProductServiceClient;
import com.vnpt.order.domain.Order;
import com.vnpt.order.domain.Product;
import com.vnpt.order.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	private ProductServiceClient productSrv;
	
	@Autowired
	private OrderService orderSrv;
	
	
	@GetMapping(value = "/create-order/{name}/{quantity}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createOrder(@PathVariable String name, @PathVariable Integer quantity) {
		try {
			Product product = productSrv.findProductByName(name);
			Order order = new Order();
			order.setProductId(product.getId());
			order.setQuantity(quantity);
			order.setTotalPrice(BigDecimal.valueOf(product.getPrice()).multiply(BigDecimal.valueOf(quantity)));
			
			Order result = orderSrv.createOrder(order);
			
			return new ResponseEntity<>("Success with id: "+result.getId() ,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResponseEntity<>("Order failed" ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
